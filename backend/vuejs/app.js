require('./bootstrap');
import Vue from 'vue';

import axios from 'axios';
import VueAxios from 'vue-axios';
import Routes from './routes/pages';
import App from './components/pages/App';
import BootstrapVue from 'bootstrap-vue'

Vue.use(VueAxios, axios);
Vue.use(BootstrapVue);

const app = new Vue({
    el: '#app',
    router: Routes,
    render: h => h(App)
});

export default app;