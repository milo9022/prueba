//===========================================================================
import Vue from 'vue';
import VueRouter from 'vue-router';
import con from '../config/index'
import PageNotFound from '../components/hola/error'
//===========================================================================



import mundo from '../components/hola/index'
import mundo2 from '../components/hola/index2'

const prefix = con.prefix;

Vue.use(VueRouter);
const router = new VueRouter({
    mode: 'history',
    routes: [ 
    {
        path: prefix + '/kd/hola',
        name: 'mundo',
        component: mundo,
        meta: {
            title:'Observaciones post-contractuales'
        }
    },
    {
        path: prefix + '/site/hola/:id',
        name: 'mundo2',
        component: mundo2,
        meta: {
            title:'Observaciones post-contractuales'
        }
    },
    { path: "*",  component: PageNotFound, meta: { title:'Sitio no encontrado' } },
    ]
});
export default router;
